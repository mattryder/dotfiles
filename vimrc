" Pathogen Loader
execute pathogen#infect()

syntax enable
set number
set mouse=a
set shell=bash      " Syntastic HATES fish cuz non-unix redirects

set autoindent
set smartindent
set smarttab

filetype plugin indent on

set tabstop=2       " The width of a TAB is set to 4.
                    " Still it is a \t. It is just that
                    " Vim will interpret it to be having
                    " a width of 4.

set shiftwidth=2    " Indents will have a width of 4

set softtabstop=2   " Sets the number of columns for a TAB

set expandtab       " Expand TABs to space

" Theme Settings - Solarized
set background=dark
colorscheme solarized
let g:solarized_termcolors=256

" Don't use Ex mode, use Q for formatting
map Q gq
let maplocalleader = ","

" NERDTree Config
autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\~$', '^\.git', '\.swp$', '\.DS_Store$', '\.o$']
let NERDTreeDirArrows=0

command W w
command Q q
